﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace WindowsFormsApp1
{
    public partial class formReservation_CheckOut : Form
    {
        Form1 parent;
        string id = "";
        DataTable dt_booking = new DataTable();
        public formReservation_CheckOut()
        {
            InitializeComponent();
            timer1.Interval = 1000 ;
            timer1.Start();
            label7.Text = DateTime.Today.ToString("dddd, dd MMMM yyyy");
            label8.Text = DateTime.Now.ToString("HH : mm : ss");

        }

        private void formReservation_CheckOut_Load(object sender, EventArgs e)
        {
            Dock = DockStyle.Fill;
            parent = (Form1)this.MdiParent;
            refreshData();
            try
            {
                OracleCommand cmd = new OracleCommand("select nama_hotel from cabang_hotel where id_hotel = '" + parent.cabang + "'", parent.oc);
                label1.Text = "CABANG "+cmd.ExecuteScalar() + "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("cabang" + ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label7.Text = DateTime.Today.ToString("dddd, dd MMMM yyyy");
            label8.Text = DateTime.Now.ToString("HH : mm : ss");
        }

        private void refreshData()
        {
            OracleDataAdapter oda = new OracleDataAdapter("select * from booking where book_status='DEBT' order by 1", parent.oc);
            dt_booking = new DataTable();
            oda.Fill(dt_booking);
            dataGridView1.DataSource = dt_booking;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = dataGridView1.CurrentCell.RowIndex;
            textBox1.Text = dataGridView1[4, row].Value + "";

            id = dataGridView1[0, row].Value + "";
            string query = "select f.nama_fasilitas,ef.jml_extra,ef.jml_extra*f.harga_fasilitas from fasilitas f, extra_fasilitas ef where ef.id_booking='" + id + "' and ef.id_fasilitas = f.id_fasilitas";
            OracleDataAdapter oda = new OracleDataAdapter(query, parent.oc);
            DataTable dt = new DataTable();
            oda.Fill(dt);
            dataGridView2.DataSource = dt;

            int total = 0;
            for (int i = 0; i < dataGridView2.Rows.Count; i++)
            {
                total += Convert.ToInt32(dataGridView2[2, i].Value);
            }
            textBox2.Text = total+"";
            textBox3.Text = textBox1.Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            textBox4.Text = "";
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            try
            {
                textBox5.Text = (Convert.ToInt32(textBox4.Text) - Convert.ToInt32(textBox3.Text)) + "";
            }
            catch (Exception)
            {
                textBox5.Text = "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(textBox5.Text) >= 0)
            {
                OracleCommand cmd = new OracleCommand("update booking set book_status='PAID' where id_booking = '" + id + "'", parent.oc);
                cmd.ExecuteNonQuery();
                textBox4.Text="";

                string bookingID = "";
                string guest = "";
                string checkin = "";
                string checkout = "";
                string noRoom = "";
                string tipe = "";
                string qtyRoom = "";
                string subtotalRoom = "";
                string subtotalExtra = "";
                string total = "";

                cmd = new OracleCommand("select g.nama from guest g, booking b where b.id_booking='" + id + "' and b.nik=g.nik", parent.oc);
                guest = cmd.ExecuteScalar()+"";
                checkin = dataGridView1[2, dataGridView1.CurrentCell.RowIndex].Value + "";
                checkout = dataGridView1[3, dataGridView1.CurrentCell.RowIndex].Value + "";
                cmd = new OracleCommand("select nomor_kamar from dbooking where id_booking = '" + id + "'", parent.oc);
                noRoom = cmd.ExecuteScalar()+"";
                cmd = new OracleCommand("select tk.jenis_kamar from tipe_kamar tk,dbooking db, kamar_hotel kh where db.id_booking = '" + id + "' and db.id_kamar = kh.id_kamar and kh.kode_tipe=tk.kode_tipe", parent.oc);
                tipe = cmd.ExecuteScalar() + "";
                cmd = new OracleCommand("select count(*) from dbooking where id_booking = '" + id + "'", parent.oc);
                qtyRoom = cmd.ExecuteScalar() + "";
                subtotalRoom = textBox1.Text;
                subtotalExtra = textBox2.Text;
                total = textBox3.Text;
                formInvoiceExtra invoExtra = new formInvoiceExtra(id);
                invoExtra.Show();
                formInvoice invo = new formInvoice(id, guest, checkin, checkout, noRoom, tipe, qtyRoom, subtotalRoom, subtotalExtra,total);
                invo.Show();
                

                OracleDataAdapter oda = new OracleDataAdapter("select id_kamar from dbooking where id_booking = '" + id + "'", parent.oc);
                DataTable dt = new DataTable();
                oda.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cmd = new OracleCommand("update kamar_hotel set status = 'OPEN' where id_kamar = '" + dt.Rows[i] + "'", parent.oc);
                    cmd.ExecuteNonQuery();
                    
                }
                cmd = new OracleCommand("delete extra_fasilitas where id_booking = '" + id + "'", parent.oc);
                cmd.ExecuteNonQuery();
                refreshData();

            }
            else
            {
                MessageBox.Show("Uang tidak cukup!");
            }
        }
    }
}
