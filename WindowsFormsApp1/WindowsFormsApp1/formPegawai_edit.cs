﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace WindowsFormsApp1
{
    public partial class formPegawai_edit : Form
    {
        public formPegawai_edit()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void formPegawai_edit_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            Form1 parent = (Form1)this.MdiParent;
            OracleCommand cmd = new OracleCommand("SELECT NAMA_HOTEL FROM CABANG_HOTEL WHERE ID_HOTEL='" + parent.cabang + "'", parent.oc);
            textBox1.Text = cmd.ExecuteScalar().ToString();
            refreshData();
            OracleDataAdapter oda = new OracleDataAdapter("SELECT * FROM JABATAN",parent.oc);
            DataTable jabatan = new DataTable();
            oda.Fill(jabatan);
            comboBox1.DataSource = jabatan;
            comboBox1.DisplayMember = "NAMA_JABATAN";
            comboBox1.ValueMember = "ID_JABATAN";
        }

        public void refreshData()
        {
            Form1 parent = (Form1)this.MdiParent;
            OracleDataAdapter oda = new OracleDataAdapter("SELECT * FROM PEGAWAI WHERE ID_HOTEL='"+parent.cabang+"'",parent.oc);
            DataTable pegawai = new DataTable();
            oda.Fill(pegawai);

            dataGridView1.DataSource = pegawai;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                MessageBox.Show("Pilih Pegawai!");
            }
            else
            {
                String nama = textBox3.Text.ToUpper();
                String telp = textBox5.Text;
                String jabatan = comboBox1.SelectedValue.ToString();
                String day = dateTimePicker1.Value.Day.ToString();
                String mon = dateTimePicker1.Value.Month.ToString();
                String year = dateTimePicker1.Value.Year.ToString();
                if (nama.Length < 2 || telp.Length < 10 || telp.Length > 13)
                {
                    MessageBox.Show("FAILED!\nTerdapat data yang tidak valid.");
                }
                else
                {
                    Form1 parent = (Form1)this.MdiParent;
                    String query = "UPDATE PEGAWAI SET ID_JABATAN='"+jabatan+"',NAMA_PEGAWAI='"+nama+ "',TGLLAHIR_PEGAWAI=TO_DATE(LPAD('" + day + "',2,'0')||'/'||LPAD('" + mon + "',2,'0')||'/'||LPAD('" + year + "',4,'0'),'DD/MM/YYYY'),TELP_PEGAWAI='" + telp+"' WHERE ID_PEGAWAI='"+textBox2.Text+"'";
                    OracleCommand cmd = new OracleCommand(query,parent.oc);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Updated!");
                    refreshData();
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int row = e.RowIndex;
                textBox2.Text = dataGridView1[0, row].Value.ToString();
                textBox3.Text = dataGridView1[3, row].Value.ToString();
                textBox5.Text = dataGridView1[5, row].Value.ToString();
                comboBox1.SelectedValue = dataGridView1[1, row].Value;
                dateTimePicker1.Value = DateTime.Parse(dataGridView1[4, row].Value.ToString());
            }
            catch (Exception)
            {}
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String id = textBox2.Text;
            Form1 parent = (Form1)this.MdiParent;
            OracleCommand cmd = new OracleCommand("DELETE FROM PEGAWAI WHERE ID_PEGAWAI='"+id+"'", parent.oc);
            cmd.ExecuteNonQuery();
            MessageBox.Show("User terhapus!");
            refreshData();
        }
    }
}
