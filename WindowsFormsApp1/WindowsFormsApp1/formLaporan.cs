﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class formLaporan : Form
    {
        Form1 parent;
        public formLaporan()
        {
            InitializeComponent();
        }

        private void formLaporan_Load(object sender, EventArgs e)
        {
            parent = (Form1)this.MdiParent;
            Dock = DockStyle.Fill;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            laporanBooking lb = new laporanBooking();
            lb.SetDatabaseLogon("pcs_proyek", "5521");
            lb.SetParameterValue("paramCin", dateTimePicker1.Value);
            lb.SetParameterValue("paramCabang", parent.cabang);
            crystalReportViewer1.ReportSource = lb;
        }
    }
}
